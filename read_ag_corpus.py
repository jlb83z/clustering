# corpus at http://groups.di.unipi.it/~gulli/AG_corpus_of_news_articles.html

import pandas as pd

file_path = "data/newsSpace1"
newfile_path = "data/newsSpace2"


def strip_CNET(line):
    return line.replace(r'CNET', 'cnet')

def write_to_new_file():
    count = 0
    with open(file_path, "r", encoding="latin-1") as file, open(newfile_path, "w", encoding="latin-1") as newfile:
        for line in file:
            end_of_record = False
            # if count >=2000000:
            #     break
            end = line.find(r'\N')
            if end > -1:
                end_of_record = True
            newline = line.replace(r'\r', '\n')
            newline = newline.rstrip("\n\r")
            # newline = strip_CNET(newline)
            if end_of_record:
                newline = newline + "\n"
            count += 1
            # if "CNET" not in newline:
                # if "Guardian" not in newline:
            newfile.write(str(newline))
    print("count:",count)
col_names = ["Field", "Type","Nothing","Key","Default","Extra","Label", "Time",]
write_to_new_file()
# df = pd.read_table(filepath_or_buffer=newfile_path,sep="\t", index_col=None ,header=0, names=["Field1", "Type1","Nothing1","Key1","Default1","Extra1"])
df = pd.read_csv(filepath_or_buffer=newfile_path,encoding="latin-1",sep="\t",index_col=False ,header=0, names=col_names, error_bad_lines=False)
# df = pd.read_csv(newfile_path,delimiter='\t',encoding='utf-8',header=None,quoting = csv.QUOTE_NONE)
# df = pd.read_fwf(filepath_or_buffer=newfile_path)
# df = pd.read_csv(filepath_or_buffer=newfile_path)

print(len(df.columns))
print(df)
for name in col_names:
    print(df[name])

df.to_pickle("data/newsSpace.pkl")

